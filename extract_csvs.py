import re, csv


VERSIONS = ["omp-for", "omp-taskloop","omp-task"]
BATCH_SIZES = ["1024"]
NUMS_THREADS = ["01","02","03","04","05","06","12","18","24","30","36","42","48"]

FILENAME = "{version}/bs_{batch_size}_thr_{num_threads}.log"
REGEX = re.compile(r" <bench> (?P<data>[\w.,\-\s]+)")

DATA_MAT = []

for version in VERSIONS:
    for batch_size in BATCH_SIZES:
        for num_threads in NUMS_THREADS:
            filename = FILENAME.format(version=version, batch_size=batch_size, num_threads=num_threads)
            try:
                with open(filename) as log:
                    iteration = 0
                    for line in log:
                        line = line.replace('\n','').split(']')
                        line = line[1 if len(line) == 2 else 0]
                        result = REGEX.match(line)
                        if result:
                            layer, step, time = result.groupdict()["data"].split(',')
                            time = float(time)
                            if step == "New Iter":
                                iteration += 1
                            DATA_MAT.append([version,batch_size,int(num_threads),iteration,layer,step,time])
            except:
                print("Arquivo", filename, "não existente.")

with open('times.csv', 'w') as csvfile:
    spamwriter = csv.writer(csvfile)
    spamwriter.writerow(["#version", "batch_size", "num_threads", "iteration", "layer", "step", "time"])
    spamwriter.writerows(DATA_MAT)
